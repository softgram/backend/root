package co.com.project.s3.adapter.outbound.aws;

import co.com.project.s3.domain.exception.S3UploadException;
import co.com.project.s3.domain.repository.S3Repository;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Slf4j
@RequiredArgsConstructor
public class S3RepositoryImpl implements S3Repository {
    private final AmazonS3 amazonClientS3;

    @Override
    public void upload(String bucketName, MultipartFile file) {
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(file.getContentType());
            metadata.setContentLength(file.getSize());
            amazonClientS3.putObject(bucketName, file.getOriginalFilename(), file.getInputStream(), metadata);
        } catch (IOException e) {
            throw new S3UploadException();
        }
    }

    @Override
    public void createBucket(String bucketName) {
        if (!amazonClientS3.doesBucketExistV2(bucketName)) {
            amazonClientS3.createBucket(bucketName);
        }
    }

    @Override
    public String getUrl(String bucketName, String key) {
        return amazonClientS3.getUrl(bucketName, key).toString();
    }
}
