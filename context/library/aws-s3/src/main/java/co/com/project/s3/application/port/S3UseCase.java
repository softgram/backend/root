package co.com.project.s3.application.port;

import org.springframework.web.multipart.MultipartFile;

public interface S3UseCase {
    void upload(String bucketName, MultipartFile file);
    void createBucket(String bucketName);
    String getUrl(String bucketName, String key);
}
