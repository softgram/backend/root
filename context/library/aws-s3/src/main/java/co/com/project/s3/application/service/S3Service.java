package co.com.project.s3.application.service;

import co.com.project.s3.application.port.S3UseCase;
import co.com.project.s3.domain.repository.S3Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class S3Service implements S3UseCase {
    private final S3Repository repository;

    @Override
    public void upload(String bucketName, MultipartFile file) {
        repository.upload(bucketName, file);
    }

    @Override
    public void createBucket(String bucketName) {
        repository.createBucket(bucketName);
    }

    @Override
    public String getUrl(String bucketName, String key) {
        return repository.getUrl(bucketName, key);
    }
}
