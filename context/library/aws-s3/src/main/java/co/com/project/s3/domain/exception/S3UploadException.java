package co.com.project.s3.domain.exception;

import co.com.project.common.domain.exception.ApplicationException;

public class S3UploadException extends ApplicationException {
    public S3UploadException() {
        super("S3 error upload exception.");
    }
}
