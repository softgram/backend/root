package co.com.project.s3.domain.repository;

import org.springframework.web.multipart.MultipartFile;

public interface S3Repository {
    void upload(String bucketName, MultipartFile file);
    void createBucket(String bucketName);
    String getUrl(String bucketName, String key);
}
