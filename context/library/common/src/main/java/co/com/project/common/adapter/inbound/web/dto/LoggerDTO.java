package co.com.project.common.adapter.inbound.web.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder(builderMethodName = "init")
public class LoggerDTO {
    private String service;
    private Integer status;
    private String message;
    private Object data;
    private String dataType;
}
