package co.com.project.common.adapter.inbound.web.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder(builderMethodName = "init")
public class Response<T> {
    protected T data;
    protected String message;
    protected Integer status;
}
