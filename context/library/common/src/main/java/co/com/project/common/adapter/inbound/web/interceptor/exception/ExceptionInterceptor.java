package co.com.project.common.adapter.inbound.web.interceptor.exception;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.common.adapter.util.ResponseBuilder;
import co.com.project.common.domain.enums.ErrorStatus;
import co.com.project.common.domain.exception.ApplicationException;
import co.com.project.common.domain.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice
public class ExceptionInterceptor {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Void> failed(Exception exception) {
        return ResponseBuilder.failed(exception);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Void> failed(RuntimeException exception) {
        return ResponseBuilder.failed(exception);
    }

    @ExceptionHandler(ApplicationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Void> failed(ApplicationException exception) {
        return ResponseBuilder.failed(exception, exception.getStatus());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<Void> failed(NotFoundException exception) {
        return ResponseBuilder.failed(exception, exception.getStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Map<String, String>> failed(MethodArgumentNotValidException exception) {
        return ResponseBuilder.failed(exception, ErrorStatus.BAD_REQUEST);
    }
}
