package co.com.project.common.adapter.inbound.web.interceptor.logger;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.common.adapter.outbound.kafka.producer.LoggerKafkaProducer;
import co.com.project.common.adapter.util.ExceptionBuilder;
import co.com.project.common.domain.model.ExceptionBody;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class LoggerInterceptor {
    private final LoggerKafkaProducer producer;
    @Value("${spring.application.name}")
    private String serviceName;

    @Around(value = "@within(org.springframework.web.bind.annotation.RestController)")
    public Object logger(ProceedingJoinPoint point) throws Throwable {
        Response<Object> response = (Response<Object>) point.proceed();
        producer.sendMessage(LoggerDTO
                                .init()
                                    .data(response.getData())
                                    .message(response.getMessage())
                                    .status(response.getStatus())
                                    .service(serviceName)
                                .build());
        return response;
    }

    @Around(value = "@within(org.springframework.web.bind.annotation.RestControllerAdvice) && args(exception)")
    public Object logger(ProceedingJoinPoint point, Exception exception) throws Throwable {
        Response<Void> response = (Response<Void>) point.proceed();
        producer.sendMessage(LoggerDTO
                                .init()
                                    .data(ExceptionBuilder.build(exception))
                                    .message(response.getMessage())
                                    .status(response.getStatus())
                                    .service(serviceName)
                                .build());
        return response;
    }
}
