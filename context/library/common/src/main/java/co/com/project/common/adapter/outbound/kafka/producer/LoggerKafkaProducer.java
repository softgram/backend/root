package co.com.project.common.adapter.outbound.kafka.producer;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.common.adapter.inbound.web.dto.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoggerKafkaProducer {
    private final KafkaTemplate<String, LoggerDTO> template;
    @Value("${spring.kafka.topics.logs}")
    private String topic;

    public void sendMessage(LoggerDTO response) {
        template.send(topic, response);
    }
}
