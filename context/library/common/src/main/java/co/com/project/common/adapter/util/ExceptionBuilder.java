package co.com.project.common.adapter.util;

import co.com.project.common.domain.model.ExceptionBody;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ExceptionBuilder {

    public ExceptionBody build(Throwable exception) {
        StackTraceElement trace = exception.getStackTrace()[0];

        return ExceptionBody
                    .init()
                        .className(trace.getClassName())
                        .fileName(trace.getFileName())
                        .line(trace.getLineNumber())
                        .cause(exception.getCause() != null ? exception.getCause().getMessage() : null)
                        .localizedMessage(exception.getLocalizedMessage())
                        .message(exception.getMessage())
                        .method(trace.getMethodName())
                        .moduleName(trace.getModuleName())
                    .build();
    }
}
