package co.com.project.common.adapter.util;

@FunctionalInterface
public interface FunctionApply {
    void apply();
}
