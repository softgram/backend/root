package co.com.project.common.adapter.util;

import co.com.project.common.adapter.inbound.web.dto.Response;
import co.com.project.common.domain.enums.ErrorStatus;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@UtilityClass
public class ResponseBuilder {

    private final String MESSAGE_SUCCESS = "Success process in server.";
    private final String MESSAGE_FAILED = "Error process in server.";

    public <T> Response<T> success(T data) {
        return Response
                    .<T>init()
                        .data(data)
                        .message(MESSAGE_SUCCESS)
                        .status(200)
                    .build();
    }

    public Response<Void> success(FunctionApply func) {
        func.apply();
        return Response
                   .<Void>init()
                        .data(null)
                        .message(MESSAGE_SUCCESS)
                        .status(204)
                    .build();
    }

    public Response<Void> failed(Throwable exception) {
        return Response
                    .<Void>init()
                        .data(null)
                        .message(MESSAGE_FAILED)
                        .status(500)
                .build();
    }

    public Response<Void> failed(Throwable exception, ErrorStatus status) {
        return Response
                    .<Void>init()
                        .data(null)
                        .message(MESSAGE_FAILED)
                        .status(status.getValue())
                    .build();
    }

    public Response<Map<String, String>> failed(MethodArgumentNotValidException exception, ErrorStatus status) {
        log.error("\n".concat(JsonUtility.toJson(ExceptionBuilder.build(exception))));

        Map<String, String> errors = new HashMap<>();

        exception.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return Response
                    .<Map<String, String>>init()
                        .data(errors)
                        .message("Error process in server")
                        .status(status.getValue())
                    .build();
    }
}
