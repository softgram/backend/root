package co.com.project.common.domain.enums;

import lombok.Getter;

@Getter
public enum ErrorStatus {
    INTERNAL_SERVER_ERROR(500),OK(200),NOT_FOUND(404),UNAUTHORIZED(401), BAD_REQUEST(400);

    private final Integer value;

    ErrorStatus(Integer value) {
        this.value = value;
    }
}
