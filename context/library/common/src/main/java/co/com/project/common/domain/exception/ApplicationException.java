package co.com.project.common.domain.exception;

import co.com.project.common.domain.enums.ErrorStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationException extends RuntimeException {
    protected final ErrorStatus status;

    public ApplicationException(String message, ErrorStatus status) {
        super(message);
        this.status = status;
    }

    public ApplicationException(String message) {
        super(message);
        this.status = ErrorStatus.INTERNAL_SERVER_ERROR;
    }
}
