package co.com.project.common.domain.exception;

import co.com.project.common.domain.enums.ErrorStatus;
import lombok.experimental.StandardException;

@StandardException
public class AuthorizationException extends ApplicationException {
    public AuthorizationException(String message) {
        super(message, ErrorStatus.UNAUTHORIZED);
    }
}
