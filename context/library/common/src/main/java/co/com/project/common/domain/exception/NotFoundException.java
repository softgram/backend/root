package co.com.project.common.domain.exception;

import co.com.project.common.domain.enums.ErrorStatus;
import lombok.experimental.StandardException;

@StandardException
public class NotFoundException extends ApplicationException {
    public NotFoundException() {
        super("Not found data in server", ErrorStatus.NOT_FOUND);
    }

    public NotFoundException(String message) {
        super(message, ErrorStatus.NOT_FOUND);
    }
}
