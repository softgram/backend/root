package co.com.project.common.domain.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder(builderMethodName = "init")
public class ExceptionBody {
    private String className;
    private String fileName;
    private Integer line;
    private String cause;
    private String localizedMessage;
    private String message;
    private String method;
    private String moduleName;
}
